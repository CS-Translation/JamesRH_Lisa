# On Designing and Deploying Internet-Scale Services

[On Designing and Deploying Internet-Scale Services](http://mvdirona.com/jrh/TalksAndPapers/JamesRH_Lisa.pdf)

英文全文在[content.md](content.md)中，请直接编辑content.md并采用中英文对照的方式翻译对应的章节。

为后续制作方便，请使用Markdown语法，参考[Markdown 语法说明 (简体中文版)](http://wowubuntu.com/markdown/)



