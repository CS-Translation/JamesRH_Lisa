On Designing and Deploying Internet-Scale Services

高可用互联网规模服务的设计与部署

James Hamilton - Windows Live Services Platform

# Abstract

# 摘要

The system-to-administrator ratio is commonly used as a rough metric to understand administrative costs in high-scale services. With smaller, less automated services this ratio can be as low as 2:1, whereas on industry leading, highly automated services, we've seen ratios as high as 2,500:1. Within Microsoft services, Autopilot [1] is often cited as the magic behind the success of the Windows Live Search team in achieving high system-to-administrator ratios. While auto-administration is important, the most important factor is actually the service itself. Is the service efficient to automate? Is it what we refer to more generally as operations-friendly? Services that are operations-friendly require little human intervention, and both detect and recover from all but the most obscure failures without administrative intervention. This paper summarizes the best practices accumulated over many years in scaling some of the largest services at MSN and Windows Live.

在大规模服务中，通常使用"系统-管理员"比来粗略的衡量管理成本。在较小、较少自动化的服务中这个比例只有2:1，而在业界领先、高度自动化的服务中这个比例高达2500:1。在微软微软的众多服务中，Autopilot [1] 经常被看成是Windows Live搜索团队能够成功达成高"系统-管理员"比背后的魔法。虽然自动化管理是重要的，而最重要的因素实际上是服务本身。服务是否高效的自动化?我们所说的服务都足够友好吗?运维友好的服务除了那些极端隐晦的，其它所有故障都几乎无需管理干预就能被自动检测并修复。本文总结了作者在MSN和Windows Live中最大的服务的扩展上多年积累的最佳实践。

# Introduction

# 引言

This paper summarizes a set of best practices for designing and developing operations-friendly services. This is a rapidly evolving subject area and, consequently, any list of best practices will likely grow and morph over time. Our aim is to help others

1.  deliver operations-friendly services quickly and

2.  avoid the early morning phone calls and meetings with unhappy customers that non-operations-friendly services tend to yield.

本文总结了一系列关于如何设计和部署具有友好操作界面服务的最佳实践。这是一个迅速发展的学科领域，因此，任何最佳实践都可能随着时间的推移而发展、变化。我们的目标是帮助其他人

1.  快速提供运维友好的服务

2.  避免服务操作复杂而不得不一大早和满怀不满的客户电话沟通或者开会。

The work draws on our experiences over the last 20 years in high-scale data-centric software systems and internet-scale services, most recently from leading the Exchange Hosted Services team (at the time, a mid-sized service of roughly 700 servers and just over 2.2M users). We also incorporate the experiences of the Windows Live Search, Windows Live Mail, Exchange Hosted Services, Live Communications Server, Windows Live Address Book Clearing House (ABCH), MSN Spaces, Xbox Live, Rackable Systems Engineering Team, and the Messenger Operations teams in addition to that of the overall Microsoft Global Foundation Services Operations team. Several of these contributing services have grown to more than a quarter billion users. The paper also draws heavily on the work done at Berkeley on Recovery Oriented Computing [2, 3] and at Stanford on Crash-Only Software [4, 5].

本文的内容来自过去20年中我们在高度数据密集型软件系统和互联网规模服务的经验，其中最近的一次是来领导Exchange托管服务团队（当时，该服务有大约700台服务器，超过2.2M用户，算是个中等规模的服务）。我们还吸收了很多团队的经验，这些团队包括Windows Live搜索、Windows Live邮箱、Exchange托管服务、实时通信服务器、Windows Live通讯录结算所（ABCH）、MSN空间、Xbox Live、美标系统工程团队、Messenger运营团队以及整个微软全球基础服务运营团队。其中有些系统服务的用户已经增长到超过250亿。本文还大量借鉴了伯克利大学在面向错误恢复计算[2，3]和斯坦福大学在Crash-Only软件[4，5]方面的工作。

Bill Hoffman [6] contributed many best practices to this paper, but also a set of three simple tenets worth considering up front:

1.  Expect failures. A component may crash or be stopped at any time. Dependent components might fail or be stopped at any time. There will be network failures. Disks will run out of space. Handle all failures gracefully.
2.  Keep things simple. Complexity breeds problems. Simple things are easier to get right. Avoid unnecessary dependencies. Installation should be simple. Failures on one server should have no impact on the rest of the data center.
3.  Automate everything. People make mistakes. People need sleep. People forget things. Automated processes are testable, fixable, and therefore ultimately much more reliable. Automate wherever possible.

Bill Hoffman [6] 不仅为本文贡献了许多最佳实践，而且还给出一组值得在设计时预先考虑的3条简单原则：

1. 可预期的失败。一个组件可能随时崩溃或者停止工作。相互依赖的的组件也可能随时失败或者停止工作。网络会出现错误，硬盘空间会被用完，需要优雅的处理所有失败。
2. 保持简单。复杂性是出现问题的根源，简单的事情更容易做对。因此要避免不必要的依赖关系、使安装过程简单，数据中心的一个服务器出现故障不能影响其它服务器。
3. 全部自动化。人会犯错，需要睡觉，会忘记东西，而自动化过程是可测的、固定的，因此会更加可靠。所以要尽可能全部自动化。

These three tenets form a common thread throughout much of the discussion that follows.

# Recommendations

# 建议

This section is organized into ten sub-sections, each covering a different aspect of what is required to design and deploy an operations-friendly service. These sub-sections include overall service design; designing for automation and provisioning; dependency management; release cycle and testing; hardware selection and standardization; operations and capacity planning; auditing, monitoring and alerting; graceful degradation and admission control; customer and press communications plan; and customer self provisioning and self help.

本节由十个子小节组成，每小节覆盖了设计和部署一个运维友好服务所需要的不同方面。这些小节包括应用程序总体设计；自动化和运营设计；依赖管理；发布周期和测试；硬件选择和标准化; 运维与能力规划； 审计、监控和报警；优雅降级和准入控制； 客户与媒体沟通计划； 客户自营和自助。

## Overall Application Design

## 应用程序总体设计

We have long believed that 80% of operations issues originate in design and development, so this section on overall service design is the largest and most important. When systems fail, there is a natural tendency to look first to operations since that is where the problem actually took place. Most operations issues, however, either have their genesis in design and development or are best solved there.

长期以来，我们认为80%的运维问题源于设计和开发，因此本节的服务总体设计是最大也是最重要的部分。当服务出现故障时，一个很自然的倾向是首先看运维，因为这是问题实际发生的地方。而实际上对于大多数运维问题来说，要么是设计和开发本身的问题，要么放到设计和开发阶段解决最好。

Throughout the sections that follow, a consensus emerges that firm separation of development, test, and operations isn't the most effective approach in the services world. The trend we've seen when looking across many services is that low-cost administration correlates highly with how closely the development, test, and operations teams work together.

在接下来的章节中会形成一种共识，即在服务的世界里把开发、测试和运维分开并不是最有效的方法。通过对很多服务的深入观察，我们看到趋势是，低成本的管理与测试、开发和运维团队协同工作的紧密程度密切相关。

In addition to the best practices on service design discussed here, the subsequent section, "Designing for Automation Management and Provisioning," also has substantial influence on service design. Effective automatic management and provisioning are generally achieved only with a constrained service model. This is a repeating theme throughout: simplicity is the key to efficient operations. Rational constraints on hardware selection, service design, and deployment models are a big driver of reduced administrative costs and greater service reliability.

除了在此讨论的关于服务设计的最佳实践之外，接下来的章节"自动化管理与运营设计"同样对服务设计有重大影响。有效的自动化管理与运营通常是通过对服务模型的一些约束来实现。这是本文中不断提及主题:简单性是高效运维的关键。对硬件选择、服务设计和部署模型的合理约束是降低管理成本和提高服务可靠性的重要驱动因素。

Some of the operations-friendly basics that have the biggest impact on overall service design are:

一些对整体服务设计影响最大的运维友好的基本原则如下:

*  Design for failure. This is a core concept when developing large services that comprise many cooperating components. Those components will fail and they will fail frequently. The components don't always cooperate and fail independently either. Once the service has scaled beyond 10,000 servers and 50,000 disks, failures will occur multiple times a day. If a hardware failure requires any immediate administrative action, the service simply won't scale cost-effectively and reliably. The entire service must be capable of surviving failure without human administrative interaction. Failure recovery must be a very simple path and that path must be tested frequently. Armando Fox of Stanford [4, 5] has argued that the best way to test the failure path is never to shut the service down normally. Just hard-fail it. This sounds counter-intuitive, but if the failure paths aren't frequently used, they won't work when needed [7].

* 针对失败做出设计。当开发一个由很多组件组成的大型服务时，这是一个核心思想。这些组件可能发生故障，且有可能频繁的发生故障。这些组件并不能总是协作的很好，并且出现故障时也不能保证对其它组件没有影响。一旦服务的服务器规模超过10000、硬盘个数超过50000,每天都会发生多次故障。如果一个硬件故障需要任何即时的人工干预，则服务在扩展时根本无法保证合适的成本和可靠性。整个服务必须能够在出现故障时自我恢复而不需要人来管理和干预。故障恢复的路径必须简单，并且该路径必须频繁测试。斯坦福大学的Armando Fox [4, 5]指出测试失败路径的最佳方法是总是非正常的关闭服务，即总是试图让服务发生故障。这听起来不符合直觉，但是如果没有频繁使用失败路径，当真正需要时它们将不能正常工作[7]。

*  Redundancy and fault recovery. The mainframe model was to buy one very large, very expensive server. Mainframes have redundant power supplies, hot-swappable CPUs, and exotic bus architectures that provide respectable I/O throughput in a single, tightly-coupled system. The obvious problem with these systems is their expense. And even with all the costly engineering, they still aren't sufficiently reliable. In order to get the fifth 9 of reliability, redundancy is required. Even getting four 9's on a single-system deployment is difficult. This concept is fairly well understood industry-wide, yet it's still common to see services built upon fragile, non-redundant data tiers. Designing a service such that any system can crash (or be brought down for service) at any time while still meeting the service level agreement (SLA) requires careful engineering. The acid test for full compliance with this design principle is the following: is the operations team willing and able to bring down any server in the service at any time without draining the work load first? If they are, then there is synchronous redundancy (no data loss), failure detection, and automatic take-over. As a design approach, we recommend one commonly used approach to find and correct potential service security issues: security threat modeling. In security threat modeling [8], we consider each possible security threat and, for each, implement adequate mitigation. The same approach can be applied to designing for fault resiliency and recovery. Document all conceivable component failures modes and combinations thereof. For each failure, ensure that the service can continue to operate without unacceptable loss in service quality, or determine that this failure risk is acceptable for this particular service (e.g., loss of an entire data center in a non-geo-redundant service). Very unusual combinations of failures may be determined sufficiently unlikely that ensuring the system can operate through them is uneconomical. Be cautious when making this judgment. We've been surprised at how frequently "unusual" combinations of events take place when running thousands of servers that produce millions of opportunities for component failures each day. Rare combinations can become commonplace.

* 冗余备份和故障恢复。主框架模型要购买一个大型、昂贵的服务器。主框架模型能提供冗余电源、支持热交换的CPU以及提供可观I/O吞吐量的外部总线架构，而且系统紧凑。这些系统的明显问题是开销太大。即使使用所有高成本的工程技术，仍然无法保证足够的可靠。为了达到五个9的可靠性，需要有冗余备份。在单系统上部署即使达到四个9也是困难的。这个概念全行业都很清楚，然而我们还是到处看到很多系统构建在脆弱的、没有数据冗余备份的设施上。设计一个允许服务随时崩溃（或者服务降级）并且满足服务级别协议（SLA）的服务需要精心的工程设计。可以通过下面的提问来判断服务是否满足这些设计原则：运维团队愿意并且能够随时停止在线的服务器而不需要事先把服务切走吗？如果是，那么系统会同步备份数据（没有数据丢失）、检查错误，并且自动接管。作为一种设计方法，我们推荐一种能找出并且纠正潜在的服务安全问题的通用方法：安全威胁模型。在安全威胁模型中[8]，会针对每个可能的安全威胁考虑分析并作出足够的应对措施。相同的方法也可以应用在故障自适应及恢复的设计上。记录所有的可以想到的组件失败模式及其组合。针对每个失败，确保服务可以正常操作，并且没有不可接受的服务质量损失，或者确定这个失败的风险对于某个特殊服务是可以接受的（比如整个数据中心的非地理冗余备份服务的失败）。你可能会认为不常见的失败组合在操作时肯定不会出现，这样做是不经济的。作出这种决策要很谨慎。当运行成千上万的服务器时，这些服务器每天有产生上百万的组件失败的机会，我们曾很奇怪这些“罕见”的组合事件频频发生。再罕见的组合事件都能成为家常便饭。

*  Commodity hardware slice. All components of the service should target a commodity hardware slice. For example, storage-light servers will be dual socket, 2- to 4-core systems in the $1,000 to $2,500 range with a boot disk. Storage-heavy servers are similar servers with 16 to 24 disks. The key observations are:
*  廉价硬件部件。所有服务组件都应该尽量使用廉价的硬件部件。例如，轻量级存储服务器可以配置为双插座、2核-4核系统以及一个启动盘的，价格在1000美元到2500美元之间；重量级存储服务器配置类似，只需要额外加16-24块磁盘。使用廉价硬件的主要观点是：
  *  large clusters of commodity servers are much less expensive than the small number of large servers they replace,
  *  使用大量廉价服务器组成的集群要比相应少量大型服务器便宜很多。
  *  server performance continues to increase much faster than I/O performance, making a small server a more balanced system for a given amount of disk,
  *  服务器性能持续提升的速度远超过I/O性能提升的速度，使用给定数量磁盘组成的小型服务器使得服务更加平衡。
  *  power consumption scales linearly with servers but cubically with clock frequency, making higher performance servers more expensive to operate, and
  *  电源的功耗随着服务器数量线性增加，但是随着时钟频率增加则是按立方的速度增加，使得高性能的服务器产生更高的运维成本。
  *  a small server affects a smaller proportion of the overall service workload when failing over.
  *  当一个小型服务器宕机时只会影响整个服务负载的较小部分。
*  Single-version software. Two factors that make some services less expensive to develop and faster to evolve than most packaged products are
  *  the software needs to only target a single internal deployment and
  *  previous versions don't have to be supported for a decade as is the case for enterprise-targeted products.
*  单一版本软件。使得一些服务比大多数打包的产品开发成本更低且进展更快的两个因素是：
  *  软件只需要一次内部部署。
  *  不需要像针对企业的产品那样对以前的版本长达数十年的支持。

    Single-version software is relatively easy to achieve with a consumer service, especially one provided without charge. But it's equally important when selling subscription-based services to non-consumers. Enterprises are used to having significant influence over their software providers and to having complete control over when they deploy new versions (typically slowly). This drives up the cost of their operations and the cost of supporting them since so many versions of the software need to be supported. The most economic services don't give customers control over the version they run, and only host one version. Holding this single-version software line requires
  1.  care in not producing substantial user experience changes release-to-release and
  2.  a willingness to allow customers that need this level of control to either host internally or switch to an application service provider willing to provide this people-intensive multi-version support.

    单一版本软件相对容易完成顾客的服务，特别是不需要提供交易的软件。但是，这在向非消费者销售订阅式的服务时同等重要。企业习惯于对他们的软件供应商有话语权，并在部署新版本（通常缓慢）时有完全的控制权。这使得他们的运维和支持成本提高，因为不得不支持众多的软件版本。最经济的服务方式是不给客户控制运行版本权利，且只管理一个版本。 保持单一版本软件线需要:
  1.  注意在发布版本之间不要发生重大用户体验的改变。
  2.  如果要满足这种对版本控制程度的客户，或者让客户自己来管理，或者让其转向一个愿意提供这种需要大量人力的多版本支持的应用服务提供商。

*  Multi-tenancy. Multi-tenancy is the hosting of all companies or end users of a service in the same service without physical isolation, whereas single tenancy is the segregation of groups of users in an isolated cluster. The argument for multi-tenancy is nearly identical to the argument for single version support and is based upon providing fundamentally lower cost of service built upon automation and large-scale.

*  多租户。多租户即把所有公司或者个人的服务部署在没有物理隔离的同一服务中，而单租户是在一个孤立的集群系统中将用户组的隔离。对于多租户的一些观点与单一版本的观点是一样的，也是基于自动化和规模化来提供更低成本的服务。

In review, the basic design tenets and considerations we have laid out above are:

*  design for failure,
*  implement redundancy and fault recovery,
*  depend upon a commodity hardware slice,
*  support single-version software, and
*  enable multi-tenancy.

综上所述，我们上面提出的设计原理和考虑的基本原则是:
* 针对失败做出设计。
* 实现冗余备份和故障恢复。
* 依赖廉价硬件部件。
* 维护单一版本软件。
* 支持多租户。

We are constraining the service design and operations model to maximize our ability to automate and to reduce the overall costs of the service. We draw a clear distinction between these goals and those of application service providers or IT outsourcers. Those businesses tend to be more people intensive and more willing to run complex, customer specific configurations.

我们通过对服务设计和运维模型的约束来最大化自动化的能力，并且减少服务的总体成本。我们详细的描绘了这些目标和那些应用服务提供上或者IT外包商之间的区别。这些企业倾向更多的人力密集型，他们更愿意使用复杂的、客户定制配置的服务。

More specific best practices for designing operations-friendly services are:

更多具体的设计运维友好的最佳实践如下:

*  Quick service health check. This is the services version of a build verification test. It's a sniff test that can be run quickly on a developer's system to ensure that the service isn't broken in any substantive way. Not all edge cases are tested, but if the quick health check passes, the code can be checked in.
*  快速的健康检查。这个是用来构建验证测试的服务版本。就像吸气测试那样可以快速的在开发者的系统上运行，以确保在任何实际操作方式下系统不会发生故障。不是所有的边缘情况都要测试，但是一旦健康检查通过，代码即可入库。
*  Develop in the full environment. Developers should be unit testing their components, but should also be testing the full service with their component changes. Achieving this goal efficiently requires single-server deployment (section 2.4), and the preceding best practice, a quick service health check.
*  在与现场相同的环境上开发。开发人员需要对开发的组件进行单元测试，但是对组件的修改需要在与现场相同的环境上测试。有效的达到这个目标需要单服务器部署（2.4）以及前面的最佳实践,即快速的健康检查。
*  Zero trust of underlying components. Assume that underlying components will fail and ensure that components will be able to recover and continue to provide service. The recovery technique is service-specific, but common techniques are to
  *  continue to operate on cached data in read-only mode or
  *  continue to provide service to all but a tiny fraction of the user base during the short time while the service is accessing the redundant copy of the failed component.

*  对底层组件零信任。针对底层组件会发生故障的情况下作出处理，保证组件能够恢复并持续提供服务。恢复的技术与服务相关，但通用的技术是：
  *  保持在只读模式下操作缓存数据。
  *  当系统访问一个发生故障的组件的备份时，除了短时间对少数用户的影响外，要能够持续对其它所有人提供服务,

*  Do not build the same functionality in multiple components. Foreseeing future interactions is hard, and fixes have to be made in multiple parts of the system if code redundancy creeps in. Services grow and evolve quickly. Without care, the code base can deteriorate rapidly.
*  不要在多个组件构建相同的功能。要预见未来的交互情况是困难的，而且如果同样代码存在冗余，修复故障通常要对系统多处进行修改。系统增长、改变的速度很快，如果不小心修改维护，则代码将会迅速腐化。
*  One pod or cluster should not affect another pod or cluster. Most services are formed of pods or sub-clusters of systems that work together to provide the service, where each pod is able to operate relatively independently. Each pod should be as close to 100% independent and without inter-pod correlated failures. Global services even with redundancy are a central point of failure. Sometimes they cannot be avoided but try to have everything that a cluster needs inside the clusters.
*  一个POD或群集不应该影响另一个POD或群集。大多数系统都是由POD格式或者子集群的形式协同工作来对外提供服务的，其中每个POD可以相对独立的操作。每个POD要尽量100%独立，并且POD故障时相互间没有影响。即使有冗余备份的全局服务都可能称为故障的中心点。有时这是不可避免的，除非使集群需要的所有东西都放在集群系统内部。
*  Allow (rare) emergency human intervention. The common scenario for this is the movement of user data due to a catastrophic event or other emergency. Design the system to never need human interaction, but understand that rare events will occur where combined failures or unanticipated failures require human interaction. These events will happen and operator error under these circumstances is a common source of catastrophic data loss. An operations engineer working under pressure at 2 a.m. will make mistakes. Design the system to first not require operations intervention under most circumstances, but work with operations to come up with recovery plans if they need to intervene. Rather than documenting these as multi-step, error-prone procedures, write them as scripts and test them in production to ensure they work. What isn't tested in production won't work, so periodically the operations team should conduct a "fire drill" using these tools. If the service-availability risk of a drill is excessively high, then insufficient investment has been made in the design, development, and testing of the tools.
*  紧急情况允许人工干预(应很少出现)。通常这种情况是因为出现灾难性事件或者其它紧急情况，这种场景需要对用户数据进行迁移。系统要设计成完全无需人工干预，但是也要理解出现组合性或者非预期故障时是需要人工干预的，只不过这些事件极少发生。这些事件会出现，而且在这种情况下操作人员的错误通常是数据丢失灾难的源头，因为运维工程师在凌晨2点且在有压力的情况下会犯错误。系统的设计首先保证大多数场景下不需要人工干预，但是也要满足需要人为干预时恢复计划操作的工作要求。不是把这些容易出现错误多个操作步骤记录在文档中，而是把这些操作步骤脚本化并在产品中测试运行以确保其能正常工作。没有经过测试的组件在产品中将无法工作，因此运维团队需要使用这些工具定期进行“消防演习”。如果演习中服务可用性的风险非常高，那肯定是在设计、开发和工具测试方面做的不够。
*  Keep things simple and robust. Complicated algorithms and component interactions multiply the difficulty of debugging, deploying, etc. Simple and nearly stupid is almost always better in a high-scale service-the number of interacting failure modes is already daunting before complex optimizations are delivered. Our general rule is that optimizations that bring an order of magnitude improvement are worth considering, but percentage or even small factor gains aren't worth it.
*  保持系统简单、强大。复杂的算法合组件交互使调试、部署等操作的难度增加数倍。简单、近似傻瓜式的系统几乎总是好于大规模的系统--在进行复杂的优化之前出现模块间交互失败模式的数量是令人望而生畏的。我们一般的处理规则是：如果优化能够给系统带来数量级的提升则是值得考虑的；否则如果只提高几个百分点甚至更少则是不值得的。
*  Enforce admission control at all levels. Any good system is designed with admission control at the front door. This follows the long-understood principle that it's better to not let more work into an overloaded system than to continue accepting work and beginning to thrash. Some form of throttling or admission control is common at the entry to the service, but there should also be admission control at all major components boundaries. Work load characteristic changes will eventually lead to sub-component overload even though the overall service is operating within acceptable load levels. See the note below in section 2.8 on the "big red switch" as one way of gracefully degrading under excess load. The general rule is to attempt to gracefully degrade rather than hard failing and to block entry to the service before giving uniform poor service to all users.
*  加强各个层级的准入控制。一个好的系统在前端都会有准入控制设计。这个遵循下面众所周知的原则：阻止过多的任务进入使得系统过载要比持续接收任务使得系统摇摇欲坠更好。有些形式的限制或者准入控制在系统的入口点是通用的，但是对所有主要组件的边界也应该有相应的准入控制。工作负载特性的变化最终会导致子组件的过载，即使系统总体的负载还运行在允许的级别之内。参考下面2.8节中关于“big red switch”作为处理过载情况下优雅服务降级的方式时的注意事项。其中，基本规则是优雅降级而不是硬性失败，并且在提供像以前一样糟糕的服务之前阻止用户访问。
*  Partition the service. Partitions should be infinitely-adjustable and fine-grained, and not be bounded by any real world entity (person, collection...). If the partition is by company, then a big company will exceed the size of a single partition. If the partition is by name prefix, then eventually all the P's, for example, won't fit on a single server. We recommend using a look-up table at the mid-tier that maps fine-grained entities, typically users, to the system where their data is managed. Those fine-grained partitions can then be moved freely between servers.
*  服务分片。服务分片应是无限可调且是细粒度的，并且不能受到任何现实世界中实体（人，集合等等）的约束。如果按公司分片，那么一个大的公司将会超过单个分片的大小；如果按名字前缀分片，比如所有名字是P开头的也不适合作为单个服务。我们推荐在中间层使用一种查找表的方式，这种方式是将细粒度的实体，典型的是用户，映射到系统中管理这些实体数据的对象上。这样，这些细粒度的分片单元就可以在服务器间自由移动了。
*  Understand the network design. Test early to understand what load is driven between servers in a rack, across racks, and across data centers. Application developers must understand the network design and it must be reviewed early with networking specialists on the operations team.
*  理解网络设计。通过尽早测试来了解机架内、机架间以及跨数据中心间负载的运行情况。应用的开发人员必须理解网络设计，并且尽早与运营团队的网络专家一起评审网络设计。
*  Analyze throughput and latency. Analysis of the throughput and latency of core service user interactions should be performed to understand impact. Do so with other operations running such as regular database maintenance, operations configuration (new users added, users migrated), service debugging, etc. This will help catch issues driven by periodic management tasks. For each service, a metric should emerge for capacity planning such as user requests per second per system, concurrent on-line users per system, or some related metric that maps relevant work load to resource requirements.
*  吞吐量分析和延迟设计。通过对核心服务用户接口的吞吐量和延迟分析来理解其对系统的影响。可以协同其它运行时操作比如数据库维护、操作配置（新用户添加、迁移）、服务调试等等来做这些分析。周期性的管理任务可以帮助捕获系统问题。对于每个服务，应该有的容量规划方面的度量，比如每系统每秒的用户请求、每系统并发在线用户，或一些将相关联的工作负载映射到资源需求有关方面的度量。
*  Treat operations utilities as part of the service. Operations utilities produced by development, test, program management, and operations should be code-reviewed by development, checked into the main source tree, and tracked on the same schedule and with the same testing. Frequently these utilities are mission critical and yet nearly untested.
*  把运维工具作为系统的一部分。开发、测试、程序管理及运维过程中开发的运维工具应该提交到主分支的代码目录树并进行研发过程的代码评审，并且和代码一起做相同跟踪和测试。这些工具至关重要，但通常没有经过测试。
*  Understand access patterns. When planning new features, always consider what load they are going to put on the backend store. Often the service model and service developers become so abstracted away from the store that they lose sight of the load they are putting on the underlying database. A best practice is to build it into the specification with a section such as, "What impacts will this feature have on the rest of the infrastructure?" Then measure and validate the feature for load when it goes live.
*  理解访问模式。当计划开发新特性时，总是会考虑它们对后端存储带来的负荷。通常服务模型和服务开发人员使用存储抽象层使得他们看不到具体的数据库，以至他们看不到新增特性对底层数据库带来的负荷。一个最佳实践是把这部分内容作为规格说明书总的一节，比如“该特新对其它基础设施的影响是什么？”，然后在该特性上线时测量和验证其对系统负荷的影响。
*  Version everything. Expect to run in a mixed-version environment. The goal is to run single version software but multiple versions will be live during rollout and production testing. Versions n and n+1 of all components need to coexist peacefully.
*  一切都要版本化。一般环境中会同时运行系统的多个版本。我们的目标是运行单一软件版本软件，但是在演示或者生成测试过程中会有多个版本存在。所有组件的版本n和版本n+1直接要能够和平共存。
*  Keep the unit/functional tests from the previous release. These tests are a great way of verifying that version n-1 functionality doesn't get broken. We recommend going one step further and constantly running service verification tests in production (more detail below).
*  保留以前版本的单元/功能测试。这些测试是验证版本n-1功能不被破坏的主要途径。我们建议更近一步并持续运行在产品中的系统验证测试（详见下面的描述）。
*  Avoid single points of failure. Single points of failure will bring down the service or portions of the service when they fail. Prefer stateless implementations. Don't affinitize requests or clients to specific servers. Instead, load balance over a group of servers able to handle the load. Static hashing or any static work allocation to servers will suffer from data and/or query skew problems over time. Scaling out is easy when machines in a class are interchangeable. Databases are often single points of failure and database scaling remains one of the hardest problems in designing internet-scale services. Good designs use fine-grained partitioning and don't support cross-partition operations to allow efficient scaling across many database servers. All database state is stored redundantly (on at least one) fully redundant hot standby server and failover is tested frequently in production.
*  避免单点故障。单点故障将导致服务或服务的一部分不可用。最好选择无状态的实现方式，不要把请求或者让客户端关联到特定的服务器上，而是通过负载均衡方式把这些请求分派到够处理这些请求的一组服务器上。静态hash或者静态的为服务器分派负载的方式随时间推移都会导致数据和/或查询负载失衡的问题。如果机架中的服务器可以互换时扩缩容是容易的。数据库经常发生单点故障，并且数据库伸缩仍然是互联网规模系统的设计中最难的问题。好的设计使用较细粒度的分片，并且不支持跨分片操作，这使得在多个数据库服务器之间可以高效伸缩。所有的数据库状态都冗余存储（至少一个）在完全的热备服务器，并且产品中故障倒换是频繁测试的。

## Automatic Management and Provisioning
## 自动管理和服务开通

Many services are written to alert operations on failure and to depend upon human intervention for recovery. The problem with this model starts with the expense of a 24x7 operations staff. Even more important is that if operations engineers are asked to make tough decisions under pressure, about 20% of the time they will make mistakes. The model is both expensive and error-prone, and reduces overall service reliability.

许多服务发生错误时仅仅是提醒用户并依赖于用户的人工干预才得以恢复。这种模式的问题随着7*24维护操作人员造成的高成本而逐渐凸显。更严重的是，如果这些操作人员在压力下做出决定的话，那么在20%的时间里将会犯错。可以说，这种人工模式成本高且易出错，同时影响整体服务的可靠性。

Designing for automation, however, involves significant service-model constraints. For example, some of the large services today depend upon database systems with asynchronous replication to a secondary, back-up server. Failing over to the secondary after the primary isn't able to service requests loses some customer data due to replicating asynchronously. However, not failing over to the secondary leads to service downtime for those users whose data is stored on the failed database server. Automating the decision to fail over is hard in this case since its dependent upon human judgment and accurately estimating the amount of data loss compared to the likely length of the down time. A system designed for automation pays the latency and throughput cost of synchronous replication. And, having done that, failover becomes a simple decision: if the primary is down, route requests to the secondary. This approach is much more amenable to automation and is considerably less error prone.

自动化的设计，很大程度上受到业务模型的约束。例如，目前为止仍有一些规模较大的服务还依赖于数据库系统并通过异步的方式回应给一个备用的次用服务。正是由于主备之间的这种异步的响应方式，一旦次用服务的失效后于主用服务，则无法处理之后的服务请求从而丢失一些用户数据。即使次用服务没有失效，也会给那些有在故障数据库存储数据的用户带来一定的停机时间。面向这种失效场景下做自动决策将是困难的，因为这依赖于人对于丢失的数据量和停机时间的判断和准确估算。还有另外一种系统的自动化设计，虽然这种它势必将会付出延迟的以及同步回应方式所带来的吞吐量减少，但是，系统失效后的决策变得非常简单：一旦主用服务失效则立即转向次用服务。这种方法将更加容易去做自动化并且更加不易出错。

Automating administration of a service after design and deployment can be very difficult. Successful automation requires simplicity and clear, easy-to-make operational decisions. This in turn depends on a careful service design that, when necessary, sacrifices some latency and throughput to ease automation. The trade-off is often difficult to make, but the administrative savings can be more than an order of magnitude in high-scale services. In fact, the current spread between the most manual and the most automated service we've looked at is a full two orders of magnitude in people costs.

在被设计和开发后的服务之上自动化管理将更加困难。自动化想要成功则需要系统的设计简单、清晰以及容易做出操作决策。反过来在设计时则需要仔细考虑，需要时，为了满足自动化的需求，还会牺牲延迟时间和吞吐量。通常这种权衡看起来很难接受，但是建立在大规模服务之上的自动化管理将会给你带来超过一个数量级的收入。事实上，对比当前那些以人工管理模式为主的服务和以自动化管理模式为主的服务，在人力成本上的付出完全不在一个数量级上。

Best practices in designing for automation include:

自动化的设计包括以下最佳实践：

*  Be restartable and redundant. All operations must be restartable and all persistent state stored redundantly.
*  可重启和冗余备份。所有操作必须可以重新开始，并且所有可持久化的状态必须具有冗余备份存储。
*  Support geo-distribution. All high scale services should support running across several hosting data centers. In fairness, automation and most of the efficiencies we describe here are still possible without geo-distribution. But lacking support for multiple data center deployments drives up operations costs dramatically. Without geo-distribution, it's difficult to use free capacity in one data center to relieve load on a service hosted in another data center. Lack of geo-distribution is an operational constraint that drives up costs.
*  多主机部署。所有大规模服务都应该运行在支持多主机多数据中心上。公平地说，自动化以及大部分我们在这里讨论的效能 很可能依然不支持多主机部署。但由于对多数据中心部署的缺乏支持，势必显著的提高了操作成本。没有多主机部署的支持，将很难利用一个数据中心的空闲资源来为另一个数据中心部署的业务减轻负担。
*  Automatic provisioning and installation. Provisioning and installation, if done by hand, is costly, there are too many failures, and small configuration differences will slowly spread throughout the service making problem determination much more difficult.
*  自动开通和安装。开通和安装，如果以手动的方式进行，成本很高，并且会有导致很多错误，即使是一个细小差别的配置，也会蔓延到业务中从而导致定位问题非常困难。
*  Configuration and code as a unit. Ensure that
  *  the development team delivers the code and the configuration as a single unit,
  *  the unit is deployed by test in exactly the same way that operations will deploy it, and
  *  operations deploys them as a unit.
*  按照一个个单元那样配置和开发。要确保：
  *  开发团队以一个个独立的单元的方式发布代码以及配置；
  *  要保证发布时的操作经过严格的测试，并且，
  *  按照单元的形式发布

    Services that treat configuration and code as a unit and only change them together are often more reliable. If a configuration change must be made in production, ensure that all changes produce an audit log record so it's clear what was changed, when and by whom, and which servers were effected (see section 2.7). Frequently scan all servers to ensure their current state matches the intended state. This helps catch install and configuration failures, detects server misconfigurations early, and finds non-audited server configuration changes.

    按照单元的形式配置和开发一些业务，同时只作为一个整体进行修改，通常会提升可靠性。如果产品中的需要修改某些配置，那么一定要有日志进行记录，所以何时修改，哪些人修改的，影响了哪些业务，这些信息都会一目了然。经常性的巡检所有服务器以确保它们的状态正常。正将帮助你及时捕获到安装和配置错误，及时发现服务错误配置的情况，同时可以帮助你找到那些没有经过审核的服务配置更改。
*  Manage server roles or personalities rather than servers. Every system role or personality should support deployment on as many or as few servers as needed.
*  管理服务的角色或者职责，而不是服务器。每个系统的角色和职责应该支持按照服务器所需要的数量发布。
*  Multi-system failures are common. Expect failures of many hosts at once (power, net switch, and rollout). Unfortunately, services with state will have to be topology-aware. Correlated failures remain a fact of life.
*  多系统故障很普遍。许多主机产生的故障立即就会被预测为电源，网络交换机和rollout。不幸的是，那些有状态的服务一定是拓扑敏感的。主机间相互影响会产生故障仍然是个不争的事实。
*  Recover at the service level. Handle failures and correct errors at the service level where the full execution context is available rather than in lower software levels. For example, build redundancy into the service rather than depending upon recovery at the lower software layer.
*  在业务层面上进行恢复。处理故障和修改错误要在能够获取完整执行上下文的业务层面上进行，而不要在低层次的软件层面上进行。举个例子，要建立业务的冗余备份而不是依赖于更底层的软件层面。
*  Never rely on local storage for non-recoverable information. Always replicate all the non-ephemeral service state.
*  对于不可恢复的信息，绝不能依靠本地存储。永远要记得复制备份那些非暂态的。
*  Keep deployment simple. File copy is ideal as it gives the most deployment flexibility. Minimize external dependencies. Avoid complex install scripts. Anything that prevents different components or different versions of the same component from running on the same server should be avoided.
*  保持部署简单。文件拷贝是一个理想的选择，因为它会带给部署最大的灵活性。最小化外部依赖。避免那些复杂的安装脚本。同时还应该避免运行在同一台服务器上的所有的那些防止多组件或者同一组件上多版本的东西。
*  Fail services regularly. Take down data centers, shut down racks, and power off servers. Regular controlled brown-outs will go a long way to exposing service, system, and network weaknesses. Those unwilling to test in production aren't yet confident that the service will continue operating through failures. And, without production testing, recovery won't work when called upon.
*  定期的使一些业务失效。例如关闭数据中心，机架下电，服务器断电。定期的可控的断电对于暴露业务/系统/网络的薄弱环节非常有帮助。对于产品的这种异常场景的测试并不一定能够代表业务将会持续的正常运行。但是一旦没有这些测试，当需要时业务很可能没办法正常恢复。

## Dependency Management
## 依赖管理

Dependency management in high-scale services often doesn't get the attention the topic deserves. As a general rule, dependence on small components or services doesn't save enough to justify the complexity of managing them. Dependencies do make sense when:

在大规模业务的很多主题中，依赖管理通常不被人关注。一般认为，小的组件或业务之间的依赖不足以说明管理它们的复杂性。依赖只有在下面的情况下才是有意义的：

*  the components being depended upon are substantial in size or complexity, or
*  被依赖的组件特别大或很复杂，或者
*  the service being depended upon gains its value in being a single, central instance.
*  被依赖业务的取值只能通过一个独立的，中心化的实体。

Examples of the first class are storage and consensus algorithm implementations. Examples of the second class of are identity and group management systems. The whole value of these systems is that they are a single, shared instance so multi-instancing to avoid dependency isn't an option.

对于第一点，典型的例子是存储和一致性算法的实现。对于第二点，典型的例子个人或群组的管理系统。这些系统最大的特点是所有的值都存在一个独立的共享的实体中，所以利用多个实体来避免依赖并不是一个选择。

Assuming that dependencies are justified according to the above rules, some best practices for managing them are:

假设以上我们谈到的关于依赖的规则是合理的话，有下面这些最佳实践：

*  Expect latency. Calls to external components may take a long time to complete. Don't let delays in one component or service cause delays in completely unrelated areas. Ensure all interactions have appropriate timeouts to avoid tying up resources for protracted periods. Operational idempotency allows the restart of requests after timeout even though those requests may have partially or even fully completed. Ensure all restarts are reported and bound restarts to avoid a repeatedly failing request from consuming ever more system resources.
*  预期的延迟。调用外部组件会消耗较长的时间。
*  Isolate failures. The architecture of the site must prevent cascading failures. Always "fail fast." When dependent services fail, mark them as down and stop using them to prevent threads from being tied up waiting on failed components.
*  ？？？？
*  Use shipping and proven components. Proven technology is almost always better than operating on the bleeding edge. Stable software is better than an early copy, no matter how valuable the new feature seems. This rule applies to hardware as well. Stable hardware shipping in volume is almost always better than the small performance gains that might be attained from early release hardware.
*  使用已发货和已验证的组件。被验证的技术永远要优于铤而走险。稳定的软件版本永远要优于那些拥有未知价值的新特性版本。同样的道理也适用于硬件。一个稳定的发货的硬件版本永远要优于那些为了提升一点点性能的新版本。
*  Implement inter-service monitoring and alerting. If the service is overloading a dependent service, the depending service needs to know and, if it can't back-off automatically, alerts need to be sent. If operations can't resolve the problem quickly, it needs to be easy to contact engineers from both teams quickly. All teams with dependencies should have engineering contacts on the dependent teams.
*  要实现内部业务的监测和警告机制。如果一个业务正在重载另一个依赖业务，该业务应该知道如果被依赖业务不能自动关闭，将要出发一次警告。如果不能通过某些操作立即解决这个问题。则应该很容易的联系到这两个业务的负责团队。所有有依赖关系的业务都应该有工程师联系到对方。
*  Dependent services require the same design point. Dependent services and producers of dependent components need to be committed to at least the same SLA as the depending service.
*  依赖服务需要具有相同的设计点。依赖的服务以及依赖组件的提供者需要满足相同的SLA。
*  Decouple components. Where possible, ensure that components can continue operation, perhaps in a degraded mode, during failures of other components. For example, rather than re-authenticating on each connect, maintain a session key and refresh it every N hours independent of connection status. On reconnect, just use existing session key. That way the load on the authenticating server is more consistent and login storms are not driven on reconnect after momentary network failure and related events.
*  将组件解耦。？？？

## Release Cycle and Testing
## 发布周期和测试

Testing in production is a reality and needs to be part of the quality assurance approach used by all internet-scale services. Most services have at least one test lab that is as similar to production as (affordably) possible and all good engineering teams use production workloads to drive the test systems realistically. Our experience has been, however, that as good as these test labs are, they are never full fidelity. They always differ in at least subtle ways from production. As these labs approach the production system in fidelity, the cost goes asymptotic and rapidly approaches that of the production system.

？？？。正因为这种实验室的方法可以保证产品被精确的验证，使得产品的成本随之逐步上升。

We instead recommend taking new service releases through standard unit, functional, and production test lab testing and then going into limited production as the final test phase. Clearly we don't want software going into production that doesn't work or puts data integrity at risk, so this has to be done carefully. The following rules must be followed:

我们建议采用新的服务发布机制，通过标准单元测试，功能性测试，以及实验室测试，然后进入小批发布阶段作为最后的测试阶段。当然我们不想让我们的软件在产品发布阶段仍然不能工作，或者将数据的完整性至于危险当中，所以这些都应该小心行事。以下这几点必须得到满足：

*  the production system has to have sufficient redundancy that, in the event of catastrophic new service failure, state can be quickly be recovered,
*  发布的产品系统必须具有足够的冗余备份能力，一旦服务发生了灾难性的故障，系统的状态能够快速恢复。
*  data corruption or state-related failures have to be extremely unlikely (functional testing must first be passing),
*  数据损毁以及相关状态故障等相关故障，绝不能发生。（首先功能性测试必须通过）
*  errors must be detected and the engineering team (rather than operations) must be monitoring system health of the code in test, and
*  错误必须被监测到。同时工程师团队（而不仅仅是维护操作）必须能够监测到系统的健康状态，同时
*  it must be possible to quickly roll back all changes and this roll back must be tested before going into production.
*  所有更改必须能够被快速回滚，这些回滚操作在发布产品之前必须是被测试过的。

This sounds dangerous. But we have found that using this technique actually improves customer experience around new service releases. Rather than deploying as quickly as possible, we put one system in production for a few days in a single data center. Then we bring one new system into production in each data center. Then we'll move an entire data center into production on the new bits. And finally, if quality and performance goals are being met, we deploy globally. This approach can find problems before the service is at risk and can actually provide a better customer experience through the version transition. Big-bang deployments are very dangerous.

以上内容听起来很危险。但是我们已经找到了能够提升产品发布用户体验的技术。并不是以尽可能快的速度发布所有产品，而是我们可以先让系统在一个数据中心先运行几天，然后再逐步发布到其他数据中心去。然后我们会将整个数据中心在新投入新版本中使用。最后，如果质量和性能目标都满足预期，我们就将全面发布。这种方法可以在服务处于风险之前就发现问题，并且在版本过度过程中提供更好的用户体验。“爆炸性”的发布是相当危险的。

Another potentially counter-intuitive approach we favor is deployment mid-day rather than at night. At night, there is greater risk of mistakes. And, if anomalies crop up when deploying in the middle of the night, there are fewer engineers around to deal with them. The goal is to minimize the number of engineering and operations interactions with the system overall, and especially outside of the normal work day, to both reduce costs and to increase quality.

？？？。

Some best practices for release cycle and testing include:

以下是一些关于发布周期和测试的最佳实践：

*  Ship often. Intuitively one would think that shipping more frequently is harder and more error prone. We've found, however, that more frequent releases have less big-bang changes. Consequently, the releases tend to be higher quality and the customer experience is much better. The acid test of a good release is that the user experience may have changed but the number of operational issues around availability and latency should be unchanged during the release cycle. We like shipping on 3-month cycles, but arguments can be made for other schedules. Our gut feel is that the norm will eventually be less than three months, and many services are already shipping on weekly schedules. Cycles longer than three months are dangerous.
*  频繁发布。通常频繁的发布会被认为是非常困难以及容易出错的。同时，我们也会发现，越是发布的频繁，版本里就会包含更少的“爆炸性”修改。因此，这样的发布则更趋向于高质量并且用户体验也会好很多。针对于一个好的产品发布，严格的测试也许会对于用户体验有影响，但是在整个产品发布周期中，围绕可用性和延迟的维护操作的数目是不会改变的。我们倾向于将发布周期设定为三个月，但这个也可以修改。直觉告诉我们实际上标准的做法应该会少于三个月，很多服务已经按周来发布了。发布周期如果超过三个月将是非常危险的。
*  Use production data to find problems. Quality assurance in a large-scale system is a data-mining and visualization problem, not a testing problem. Everyone needs to focus on getting the most out of the volumes of data in a production environment. A few strategies are:
  *  Measurable release criteria. Define specific criteria around the intended user experience, and continuously monitor it. If availability is supposed to be 99%, measure that availability meets the goal. Both alert and diagnose if it goes under.
  *  Tune goals in real time. Rather than getting bogged down deciding whether the goal should be 99% or 99.9% or any other goal, set an acceptable target and then ratchet it up as the system establishes stability in production.
  *  Always collect the actual numbers. Collect the actual metrics rather than red and green or other summary reports. Summary reports and graphs are useful but the raw data is needed for diagnosis.
  *  Minimize false positives. People stop paying attention very quickly when the data is incorrect. It's important to not over-alert or operations staff will learn to ignore them. This is so important that hiding real problems as collateral damage is often acceptable.
  *  Analyze trends. This can be used for predicting problems. For example, when data movement in the system diverges from the usual rate, it often predicts a bigger problem. Exploit the available data.
  *  Make the system health highly visible. Require a globally available, real-time display of service health for the entire organization. Have an internal website people can go at any time to understand the current state of the service.
  *  Monitor continuously. It bears noting that people must be looking at all the data every day. Everyone should do this, but make it the explicit job of a subset of the team to do this.
*  使用真实产品的数据去发现问题。在大规模系统里面，质量保证问题并不是一个测试的问题，而是一个数据挖掘以及呈现的问题。每个人都应该关注在生产环境中获取大量的真实数据。下面是一些策略：
  *  可衡量的发布标准。围绕预期的用户体验定义特定的标准,并持续监测它。假设我们系统的可用性要达到99%，则应该持续的监测该指标是否达到目标。一旦没有达到则立即出发告警并诊断问题。
  *  实时优化目标值。并不需要纠结于是把目标设置为99.9%还是99.99%或者其他什么值。先设置一个可接受的目标，然后当系统更加稳定之后逐步提升该值。
  *  总是收集实际的数据。收集实际的指标数字，而不是“通过或不通过”或者其他总结报告。总结报告或者图表虽然也有用，但裸数据对于诊断问题更有意义。
  *  减少误报。当数据不正确时，很快维护人员便不再关注了。因此非常重要的是不能过多上报无用的告警。维护人员会因此忽略它们。甚至将真实的问题作为附加错误上报都是可接受的。
  *  分析趋势。这可以用于预测问题。例如，某些指标的改变速度超出了常规，这通常预示着有重大问题要发生。因此要利用好现有的这些数据。
  *  让系统的健康状况高度可视化。需要一个全范围可用的，实时显示整个组织的服务健康状况。有一个内部人员可以随时去了解目前的服务状态。
  *  持续监测。有一点至关重要，人们必须每天都能够看到所有数据。每个人都可以做这些，但是要把这个工作明确下来分给团队中的某个小团队去做。
*  Invest in engineering. Good engineering minimizes operational requirements and solves problems before they actually become operational issues. Too often, organizations grow operations to deal with scale and never take the time to engineer a scalable, reliable architecture. Services that don't think big to start with will be scrambling to catch up later.
*  在工程设计上投资。好的工程设计可以减少对维护工作的需求。同时还可以在成为维护问题之前就能发现问题，通常 我们会增加维护操作来应对规模上的变化，但是很少会设计一套可扩展的，可靠的架构。起初并不认为有很大规模的服务随后在规模上便会迎头赶上。
*  Support version roll-back. Version roll-back is mandatory and must be tested and proven before roll-out. Without roll-back, any form of production-level testing in very high risk. Reverting to the previous version is a rip cord that should always be available on any deployment.
*  支持版本回退。版本回退是个强制性的要求。并且在版本发布之前就应该被验证过的。没有版本回退，任何形式的产品级别的测试都是高风险的。回退版本的功能就像是一个降落伞一样，在任何部署的场景下都必须可用。
*  Maintain forward and backward compatibility. This vital point strongly relates to the previous one. Changing file formats, interfaces, logging/ debugging, instrumentation, monitoring and contact points between components are all potential risk. Don't rip out support for old file formats until there is no chance of a roll back to that old format in the future.
*  保持向前和向后兼容性。这个关键点和上一个点密切相关。改变组件间的文件格式，接口，日志及调试，监测，以及交互点等等都是有潜在的风险。不要结束对旧文件的支持，除非未来完全没有机会回到旧文件的格式。
*  Single-server deployment. This is both a test and development requirement. The entire service must be easy to host on a single system. Where single-server deployment is impossible for some component (e.g., a dependency on an external, non-single box deployable service), write an emulator to allow single-server testing. Without this, unit testing is difficult and doesn't fully happen. And if running the full system is difficult, developers will have a tendency to take a component view rather than a systems view.
*  单服务器部署。这一点即是一个测试需求也是一个部署需求。一个完整的服务必须能够轻松的被部署在一个节点上。如果有组件没有办法实施单服务器部署（例如，依赖于另一个外部的非单服务器部署的服务），应该做一个模拟器去执行单服务器的测试。不然的话，单元测试将非常困难而且无法全面覆盖。如果运行完整的系统是困难的，开发人员可以趋向于采取组件视图而不是系统视图。
*  Stress test for load. Run some tiny subset of the production systems at twice (or more) the load to ensure that system behavior at higher than expected load is understood and that the systems don't melt down as the load goes up.
*  针对登录做压力测试。仅运行系统的某些部分并尝试两次或多次登录操作以确保系统行为在超预期登录的场景下是可理解的。并且要保证系统在登录数逐步增加的情况下不会崩溃。
*  Perform capacity and performance testing prior to new releases. Do this at the service level and also against each component since work load characteristics will change over time. Problems and degradations inside the system need to be caught early.
*  在发布新版本之前做处理能力测试和压力测试。在业务层面上对每一个组件做这样的测试，因为工作的负载会随着时间变化。故障以及系统处理能力的退化需要提早发现。
*  Build and deploy shallowly and iteratively. Get a skeleton version of the full service up early in the development cycle. This full service may hardly do anything at all and may include shunts in places but it allows testers and developers to be productive and it gets the entire team thinking at the user level from the very beginning. This is a good practice when building any software system, but is particularly important for services.
* 小范围迭代式的构建和发布。在开发周期早期便获得完整服务的主干分支版本。虽然这样一个包含所有服务的版本还不能做任何事情，但是这样可以使开发人员和测试人员开始的时候就在用户的层面上思考问题。这一点对于构建任何软件系统都是有益的时间，尤其是对于服务则更加重要。
*  Test with real data. Fork user requests or workload from production to test environments. Pick up production data and put it in test environments. The diverse user population of the product will always be most creative at finding bugs. Clearly, privacy commitments must be maintained so it's vital that this data never leak back out into production.
*  用真实数据进行测试。提取产品的用户请求或工作负载到测试环境中去。抓取实际产品环境中的数据到测试环境中。拥有不同用户群体的产品在发现错误上永远是最有创造性的。很显然，用户数据的隐私一定要保证安全。同时很关键的一点是不能将数据泄漏到生产环境中。
*  Run system-level acceptance tests. Tests that run locally provide sanity check that speeds iterative development. To avoid heavy maintenance cost they should still be at system level.
*  运行系统级别的验收测试。本地运行的测试可以为产品提供验证并加速迭代开发。为了降低沉重的维护成本该测试应该始终保持在系统级别。
*  Test and develop in full environments. Set aside hardware to test at interesting scale. Most importantly, use the same data collection and mining techniques used in production on these environments to maximize the investment.
*  在安全环境中测试和开发。将额外的硬件用于测试令人关注的领域。更重要的是，在这些环境中使用在产品中相同的数据采集和挖掘技术，从而将投资收益最大化。

## Hardware Selection and Standardization
## 硬件选型与标准化

The usual argument for SKU standardization is that bulk purchases can save considerable money. This is inarguably true. The larger need for hardware standardization is that it allows for faster service deployment and growth. If each service is purchasing their own private infrastructure, then each service has to:

对于SKU标准化通常的说法是，批量采购可以节省大量的金钱。确实如此，但对于硬件标准化更大的需求是，它允许更快的服务部署和增长。如果每个服务都拥有自己的一套结构，那么每个服务就不得不：

*  determine which hardware currently is the best cost/performing option,
*  决策目前哪款硬件在经济上是最优选择。
*  order the hardware, and
*  将这些硬件排序，以及
*  do hardware qualification and software deployment once the hardware is installed in the data center.
*  一旦硬件安装在数据中心后需要进行硬件资格恒和软件部署工作。

This usually takes a month and can easily take more.

通常会花费一个月甚至更多的时间。

A better approach is a "services fabric" that includes a small number of hardware SKUs and the automatic management and provisioning infrastructure on which all service are run. If more machines are needed for a test cluster, they are requested via a web service and quickly made available. If a small service gets more successful, new resources can be added from the existing pool. This approach ensures two vital principles:

更好的方式是这样一种“服务结构”，它包括了少量的硬件SKU以及自动化管理，以及所有服务都需要的基础架构的提供。如果某些测试场景需要更多的资源，则可以通过web服务快速的获取并且应用起来。如果一个小的服务成功获取了更多的资源，那么新的资源可以添加到现有的资源池中。这种方法要保证两条原则：

*  all services, even small ones, are using the automatic management and provisioning infrastructure and
*  所有的服务，即使是很小的服务，都要使用这套自动化管理和基础架构；
*  new services can be tested and deployed much more rapidly.
*  新增的服务应该能够被测试以及部署应该非常迅速。

Best practices for hardware selection include:

硬件选型方面的最佳实践包括：

*  Use only standard SKUs. Having a single or small number of SKUs in production allows resources to be moved fluidly between services as needed. The most cost-effective model is to develop a standard service-hosting framework that includes automatic management and provisioning, hardware, and a standard set of shared services. Standard SKUs is a core requirement to achieve this goal.
*  只使用标准化的SKU。产品中拥有单个或者少量SKU可以使得服务间资源的调配更加流畅。最实惠的模式是开发一个标准的服务托管框架，包括自动管理和配置，硬件，和一套标准的共享服务。那么SKU的标准化则是完成这一目标的核心需求。
*  Purchase full racks. Purchase hardware in fully configured and tested racks or blocks of multiple racks. Racking and stacking costs are inexplicably high in most data centers, so let the system manufacturers do it and wheel in full racks.
*  购买完整机架。购买硬件时需要确定机架或者多机架都满配置的情况是被验证过的。机架和层架在大部分数据中心中是成本最高的部分，让系统制造商去做这个事情并且购买完整的机架。
*  Write to a hardware abstraction. Write the service to an abstract hardware description. Rather than fully exploiting the hardware SKU, the service should neither exploit that SKU nor depend upon detailed knowledge of it. This allows the 2-way, 4-disk SKU to be upgraded over time as better cost/performing systems come available. The SKU should be a virtual description that includes number of CPUs and disks, and a minimum for memory. Finer-grained information about the SKU should not be exploited.
*  向硬件抽象写入。将服务写入一个抽象的硬件描述。并不是完全运用硬件SKU，服务应该既不完全运用硬件SKU也不依赖相关细节。这样则会允许双通道，4磁盘的SKU随着时间的推移而被升级，因为成本和性能上都表现更好的SKU已经出现。SKU仅仅是个虚拟的描述，包括CPU和磁盘的数量，以及最小的内存。关于SKU更细粒度的信息将不会被应用。
*  Abstract the network and naming. Abstract the network and naming as far as possible, using DNS and CNAMEs. Always, always use a CNAME. Hardware breaks, comes off lease, and gets repurposed. Never rely on a machine name in any part of the code. A flip of the CNAME in DNS is a lot easier than changing configuration files, or worse yet, production code. If you need to avoid flushing the DNS cache, remember to set Time To Live sufficiently low to ensure that changes are pushed as quickly as needed.
*  将网络和命名抽象。使用DNS和CNAME尽可能的抽象网络和命名，永远，永远要记得使用CNAME。硬件坏了，可能会租赁并使用。再代码的任何部分都不应该依赖机器名。应用DNS中的CNAME要比修改配置文件容易的多，更差的方式是修改产品代码。如果你需要避免刷新DNS缓存，记得设置生存时间足够低以确保变化生效的足够迅速。

## Operations and Capacity Planning
## 运维与能力规划

The key to operating services efficiently is to build the system to eliminate the vast majority of operations administrative interactions. The goal should be that a highly-reliable, 24x7 service should be maintained by a small 8x5 operations staff.

有效运维的关键是消除绝大多数运维管理交互。 目标应该是由5x8小型运维团队维护高可靠性的全天候服务。

However, unusual failures will happen and there will be times when systems or groups of systems can’t be brought back on line. Understanding this possibility, automate the procedure to move state off the damaged systems. Relying on operations to update SQL tables by hand or to move data using ad hoc techniques is courting disaster. Mistakes get made in the heat of battle. Anticipate the corrective actions the operations team will need to make, and write and test these procedures up-front. Generally, the development team needs to automate emergency recovery actions and they must test them. Clearly not all failures can be anticipated, but typically a small set of recovery actions can be used to recover from broad classes of failures. Essentially, build and test "recovery kernels" that can be used and combined in different ways depending upon the scope and the nature of the disaster.

然而，非正常失效的发生有时会导致系统不能恢复在线。了解这种可能性，并使关闭受损系统的过程自动化。依赖于手动更新SQL表或者使用ad hoc技术来迁移数据将招致灾难。错误更容易产生在激战的气氛中。预测运维团队需要进行的纠正措施并提前编写、测试这些过程。一般来说，开发团队需要自动化紧急恢复动作并对其进行测试。显然不是所有的故障都可以预期，但通常可以使用一小组恢复动作来恢复多数类别的故障。最根本的是建立和测试“恢复内核”，以便根据灾难的范围和性质以不同的方式使用和组合。

The recovery scripts need to be tested in production. The general rule is that nothing works if it isn’t tested frequently so don’t implement anything the team doesn’t have the courage to use. If testing in production is too risky, the script isn’t ready or safe for use in an emergency. The key point here is that disasters happen and it’s amazing how frequently a small disaster becomes a big disaster as a consequence of a recovery step that doesn’t work as expected. Anticipate these events and engineer automated actions to get the service back on line without further loss of data or up time.

恢复脚本需要在生产环境中测试。 一般规则是，如果没有经常测试什么也不会工作，所以不要做团队没有勇气做的任何事情。如果脚本在生产环境中进行测试风险太大的话，则亦不能在紧急情况下安全地使用。关键在于，灾难发生时，一系列未按预期工作的恢复步骤使得小灾难发展成为巨大的灾难的情况发生得如此之频繁。 预测这些事件，并设计自动化恢复服务的操作，避免进一步丢失数据或延长离线时间。


*  <B>Make the development team responsible.</B> Amazon is perhaps the most aggressive down this path with their slogan "you built it, you manage it." That position is perhaps slightly stronger than the one we would take, but it's clearly the right general direction. If development is frequently called in the middle of the night, automation is the likely outcome. If operations is frequently called, the usual reaction is to grow the operations team.
*  使开发团队负责任。亚马逊可能是这条道路最积极的拥护者，他们的口号是“谁构建，谁管理”。这可能比我们做得更激进，然而这显然是正确的大方向。如果经常在半夜呼叫开发人员，那么自动化可能产生。如果经常在半夜呼叫运维人员，通常的反应则是扩大运维团队。

*  <B>Soft delete only.</B> Never delete anything. Just mark it deleted. When new data comes in, record the requests on the way. Keep a rolling two week (or more) history of all changes to help recover from software or administrative errors. If someone makes a mistake and forgets the where clause on a delete statement (it has happened before and it will again), all logical copies of the data are deleted. Neither RAID nor mirroring can protect against this form of error. The ability to recover the data can make the difference between a highly embarrassing issue or a minor, barely noticeable glitch. For those systems already doing off-line backups, this additional record of data coming into the service only needs to be since the last backup. But, being cautious, we recommend going farther back anyway.
*  仅软删除。不要删除任何内容 只要标记为已删除。 当新数据进来时，在途中记录请求。 保留所有更改的两个星期（或更多）历史记录，以帮助从软件或管理错误中恢复。 如果有人犯了一个错误，并忘记了delete语句的where子句（它在之前已经发生并且将再次发生），则会删除所有数据的逻辑副本。 RAID和镜像都不能防止这种形式的错误。 恢复数据的能力可以使高度尴尬的问题或次要的，几乎不可察觉的毛刺之间的差异。 对于已经进行离线备份的那些系统，进入服务的数据的这个附加记录只需要是自上次备份以来。 但是，谨慎，我们建议再往前走。

*  <B>Track resource allocation.</B> Understand the costs of additional load for capacity planning. Every service needs to develop some metrics of use such as concurrent users online, user requests per second, or something else appropriate. Whatever the metric, there must be a direct and known correlation between this measure of load and the hardware resources needed. The estimated load number should be fed by the sales and marketing teams and used by the operations team in capacity planning. Different services will have different change velocities and require different ordering cycles. We've worked on services where we updated the marketing forecasts every 90 days, and updated the capacity plan and ordered equipment every 30 days.
*  跟踪资源分配。了解容量规划的额外负载的成本。每个服务都需要开发一些使用指标，例如在线并发用户，每秒用户请求或其他适当的指标。无论什么度量，在该负载度量和所需的硬件资源之间必须存在直接和已知的相关性。估计负载数应由销售和营销团队提供，并由运营团队在容量规划中使用。不同的服务将具有不同的变化速度并且需要不同的订购周期。我们每90天为我们的服务更新市场预测，每30天更新容量计划并订购设备。

*  <B>Make one change at a time.</B> When in trouble, only apply one change to the environment at a time. This may seem obvious, but we've seen many occasions when multiple changes meant cause and effect could not be correlated.
*  一次只有一项变更。遇到问题时，一次只能对环境应用一项更改。虽然道理很明显，但我们仍然在很多场合看到多重变化引起的原因和结果不能相关。

*  <B>Make everything configurable.</B> Anything that has any chance of needing to be changed in production should be made configurable and tunable in production without a code change. Even if there is no good reason why a value will need to change in production, make it changeable as long as it is easy to do. These knobs shouldn't be changed at will in production, and the system should be thoroughly tested using the configuration that is planned for production. But when a production problem arises, it is always easier, safer, and much faster to make a simple configuration change compared to coding, compiling, testing, and deploying code changes.
*  使一切都可配置。任何有可能在生产环境中需要的变化都应该在生产环境中可以进行配置和调整，而不需要更改代码。 即使没有什么好的理由解释为什么一个值需要在生产环境中改变，只要容易做到，就让它可以改变。 这些配置值不应该在生产中随意更改，系统应该使用计划用于生产的配置进行彻底测试。但是，当生产环境出现问题时，简单的配置更改与编码、编译、测试和部署更改相比，总能更容易、更安全也更快地进行。

## Auditing, Monitoring and Alerting
## 审计、监控和警报

The operations team can't instrument a service in deployment. Make substantial effort during development to ensure that performance data, health data, throughput data, etc. are all produced by every component in the system.

运维团队无法在生产环境中度量服务。因此开发阶段就必须努力确保每个组件能产生各自的性能、健康度、吞吐量等数据。

Any time there is a configuration change, the exact change, who did it, and when it was done needs to be logged in the audit log. When production problems begin, the first question to answer is what changes have been made recently. Without a configuration audit trail, the answer is always "nothing" has changed and it's almost always the case that what was forgotten was the change that led to the question.

对于任何一项配置变更，确切的更改、谁做的、什么时候完成的，都必须记录在审计日志中。当生产环境出现状况时，第一个要回答的问题就是最近做了哪些改动。如果没有配置审计跟踪，那么答案总是没有任何变更，而几乎总是那些被遗忘了的变更导致了问题。

Alerting is an art. There is a tendency to alert on any event that the developer expects they might find interesting and so version-one services often produce reams of useless alerts which never get looked at. To be effective, each alert has to represent a problem. Otherwise, the operations team will learn to ignore them. We don't know of any magic to get alerting correct other than to interactively tune what conditions drive alerts to ensure that all critical events are alerted and there are not alerts when nothing needs to be done. To get alerting levels correct, two metrics can help and are worth tracking: 1) alerts-to-trouble ticket ratio (with a goal of near one), and 2) number of systems health issues without corresponding alerts (with a goal of near zero).

警报乃艺术。开发人员倾向于对自己可能感兴趣的任何事件发出警报，因此服务的第一个版本总是会产生大量根本不会有人看的无用警报。每个警报都必须表示一个问题，这才是有效的警报，否则，运维团队很快就会忽略警报。我们没有任何比交互式调整警报驱动条件更有效的方法来确保对所有关键事件发生警报同时又不发生任何误报（用误报来替代那么长的句子是否过于简单？）。有两个指标可以帮助我们跟踪和确认警告级别是否正确：1、警报故障比（目标是趋向1）；2、漏报问题数（目标是接近0）。

*  Instrument everything. Measure every customer interaction or transaction that flows through the system and report anomalies. There is a place for "runners" (synthetic workloads that simulate user interactions with a service in production) but they aren't close to sufficient. Using runners alone, we've seen it take days to even notice a serious problem, since the standard runner workload was continuing to be processed well, and then days more to know why.

*  度量一切。测量流经系统的每一个客户交互或事务，并报告异常情况。虽然有“跑步者”（模拟生产中用户与服务交互的虚拟负载），但仍然不足够接近真实情况。仅有跑步者，我们需要好几天才能注意到存在的严重问题，因为标准跑步者的工作负载被处理得很好，然而天知道发生了什么。

*  Data is the most valuable asset. If the normal operating behavior isn't well-understood, it's hard to respond to what isn't. Lots of data on what is happening in the system needs to be gathered to know it really is working well. Many services have gone through catastrophic failures and only learned of the failure when the phones started ringing.

*  数据是最宝贵的资产。如果正常的操作行为没有被很好地理解，那么很难对非正常操作行为做出反应。必须收集系统中产生的大量数据用来确认系统确实工作良好。许多服务都经历了灾难性的故障，但只有当报警电话响起时才知道故障。

*  Have a customer view of service. Perform end-to-end testing. Runners are not enough, but they are needed to ensure the service is fully working. Make sure complex and important paths such as logging in a new user are tested by the runners. Avoid false positives. If a runner failure isn't considered important, change the test to one that is. Again, once people become accustomed to ignoring data, breakages won't get immediate attention.

*  以客户视角看服务。虽然执行端到端测试和跑步者仍然不够，但它们对确保服务完全正常工作是必不可少的。确保复杂和重要的路径被跑步者进行过测试，例如注册新用户。避免误报。确保所有跑步者测试都是有意义的，失败都必须被重视。再次强调，一旦人们习惯于忽略数据，损坏将不会立即引起注意。

*  Instrument for production testing. In order to safely test in production, complete monitoring and alerting is needed. If a component is failing, it needs to be detected quickly.

*  度量生产测试。为了在生产环境中安全测试，需要完整的监控和警报。如果组件出现故障，需要能快速检测出来。 

*  Latencies are the toughest problem. Examples are slow I/O and not quite failing but processing slowly. These are hard to find, so instrument carefully to ensure they are detected.

*  延迟是最棘手的问题。例如低效的IO，并非失败，但导致处理缓慢。这种问题一般很难定位，所以要仔细度量，以确保它们被检测到。 

*  Have sufficient production data. In order to find problems, data has to be available. Build fine-grained monitoring in early or it becomes expensive to retrofit later. The most important data that we've relied upon includes:

*  拥有足够的生产数据。为了发现问题，数据必须可用。早期就要建立起细粒度监控，否则后期再改进将付出很高的成本。我们依赖的最重要的数据包括： 

  *  Use performance counters for all operations. Record the latency of operations and number of operations per second at the least. The waxing and waning of these values is a huge red flag.

  *  对所有操作进行性能计数。至少记录操作的延迟和每秒的操作数。这些值的亏盈就重要的标记（这句完全不懂）。 

  *  Audit all operations. Every time somebody does something, especially something significant, log it. This serves two purposes: first, the logs can be mined to find out what sort of things users are doing (in our case, the kind of queries they are doing) and second, it helps in debugging a problem once it is found. A related point: this won't do much good if everyone is using the same account to administer the systems. A very bad idea but not all that rare.

  *  审计所有操作。每件事情，特别是重要的事情都要记录下来。这样做有两个目的：首先，这可以挖掘日志以找出用户正在做什么样的事情（以我们的产品为例，即他们在查询什么）；其次，一旦发现问题后这可以帮助调试。与此相关的是：如果每个人都使用相同的帐户来管理系统，上述作用很难发挥。 使用相同帐户的主意很糟糕，但并不罕见。 

  *  Track all fault tolerance mechanisms. Fault tolerance mechanisms hide failures. Track every time a retry happens, or a piece of data is copied from one place to another, or a machine is rebooted or a service restarted. Know when fault tolerance is hiding little failures so they can be tracked down before they become big failures. We had a 2000-machine service fall slowly to only 400 available over the period of a few days without it being noticed initially.

  *  跟踪所有容错机制。容错机制会隐藏故障。跟踪每次重试发生、每片数据的复制、每次机器重新启动或服务重新启动。理解容错何时会隐藏小的故障，以便在它们变成大的故障前被跟踪到。我们有一次在几天的时间内，2000台机器的服务缓慢下降到只有400台可用，就因为最初没有注意到。 

  *  Track operations against important entities. Make an "audit log" of everything significant that has happened to a particular entity, be it a document or chunk of documents. When running data analysis, it's common to find anomalies in the data. Know where the data came from and what processing it's been through. This is particularly difficult to add later in the project.

  *  跟踪对重要实体的操作。对特定实体发生的一切重要事件（无论是一个文档还是一组文档）记录“审计日志”。一般在执行数据分析时，总能在数据中发现异常。记录数据来自哪里以及进行过什么处理。这在以后非常难以添加。 

  *  Asserts. Use asserts freely and throughout the product. Collect the resulting logs or crash dumps and investigate them. For systems that run different services in the same process boundary and can't use asserts, write trace records. Whatever the implementation, be able to flag problems and mine frequency of different problems.

  *  断言。在整个产品中自由使用断言。收集因而生成的日志或崩溃转储并审查。对于因不同服务运行在同一进程而不能使用断言的系统，把这些信息写入跟踪记录。无论如何实现，它都必做能够标记问题以及采集不同问题的频繁率。 

  *  Keep historical data. Historical performance and log data is necessary for trending and problem diagnosis.

  *  保留历史数据。历史性能和日志数据对于趋势和问题诊断是必须的。 

*  Configurable logging. Support configurable logging that can optionally be turned on or off as needed to debug issues. Having to deploy new builds with extra monitoring during a failure is very dangerous.

*  可配置的日志。支持可配置的日志记录，可以根据需要选择打开或关闭以调试问题。在故障期间部署新构建的版本以增加额外监视是非常危险的。 

*  Expose health information for monitoring. Think about ways to externally monitor the health of the service and make it easy to monitor it in production.

*  公开用于监视的健康信息。考虑外部监视服务健康状况的途径，并使得在生产环境中易于实现。 

*  Make all reported errors actionable. Problems will happen. Things will break. If an unrecoverable error in code is detected and logged or reported as an error, the error 
message should indicate possible causes for the error and suggest ways to correct it. Un-actionable error reports are not useful and, over time, they get ignored and real failures will be missed.

*  所有报告的错误应当是可操作。问题一定会发生，事情一定会打断。如果代码中检测到不可恢复的错误，那么请记录或报告为错误，错误消息应当指示错误的可能原因，并给出纠正错误的建议方法。报告不可操作的错误是没有用的，随着时间的推移，这些错误将被忽略，同时真正的故障也将错过。 

*  Enable quick diagnosis of production problems.

*  要能快速诊断生产问题。

  *  Give enough information to diagnose. When problems are flagged, give enough information that a person can diagnose it. Otherwise the barrier to entry will be too high and the flags will be ignored. For example, don't just say "10 queries returned no results." Add "and here is the list, and the times they happened."

  *  提供足够的信息用以诊断。标记问题时，要给出诊断问题的足够的信息。否则，诊断问题的门槛变得太高，标记的问题将被忽略。例如，不要只说“10个查询返回没有结果。”或者“这是列表和它们发生的时间。”

  *  Chain of evidence. Make sure that from beginning to end there is a path for developer to diagnose a problem. This is typically done with logs.

  *  证据链。确保从头到尾有开发人员诊断问题的途径。这通常用日志完成。 

  *  Debugging in production. We prefer a model where the systems are almost never touched by anyone including operations and that debugging is done by snapping the image, dumping the memory, and shipping it out of production. When production debugging is the only option, developers are the best choice. Ensure they are well trained in what is allowed on production servers. Our experience has been that the less frequently systems are touched in production, the happier customers generally are. So we recommend working very hard on not having to touch live systems still in production.

  *  别在生产环境中调试。我们喜欢这样一种模型，系统几乎从来不被任何人接触，包括运维，一般通过捕捉镜像、转存内存并转移到生产环境外进行调试。当生产环境调试是唯一选择时，开发人员是最佳人选。确保他们接受过在生产环境中允许做哪些操作的培训。我们的经验是，接触生产环境的频率越低，客户的满意度越高。因此，我们建议不接触生产环境的目标值得我们努力。 

  *  Record all significant actions. Every time the system does something important, particularly on a network request or modification of data, log what happened. This includes both when a user sends a command and what the system internally does. Having this record helps immensely in debugging problems. Even more importantly, mining tools can be built that find out useful aggregates, such as, what kind of queries are users doing (i.e., which words, how many words, etc.)

  *  记录所有重要操作。每次系统做一些重要的事情，特别是在网络请求或数据修改时，记录发生了什么。包括用户发送命令的时间以及系统内部活动。拥有此记录对调试问题大有帮助。更重要的是，以此构建挖掘工具来找出有用的聚合信息，例如，用户做什么样的查询（例如，哪些单词，多少单词等） 

## Graceful Degradation and Admission Control
## 优雅降级和准入控制

There will be times when DOS attacks or some change in usage patterns causes a sudden workload spike. The service needs be able to degrade gracefully and control admissions. For example, during 9/11 most news services melted down and couldn't provide a usable service to any of the user base. Reliably delivering a subset of the articles would have been a better choice. Two best practices, a "big red switch" and admission control, need to be tailored to each service. But both are powerful and necessary.

DOS攻击或者使用模式的某些变化有时会导致工作负载突然出现洪峰。此时服务需要能够优雅地降级或进行控制访问。例如，在9/11期间，大多数新闻服务崩溃，不能向任何用户群提供可用服务。此时，能可靠地递送部分文章或许是一个更好的选择。这儿有两个最佳实践需要每个服务来考虑：一个是“big red switch”，另一个是准入控制。两者都是很强大和很必要的。

*  Support a "big red switch." The idea of the "big red switch" originally came from Windows Live Search and it has a lot of power. We've generalized it somewhat in that more transactional services differ from Search in significant ways. But the idea is very powerful and applicable anywhere. Generally, a "big red switch" is a designed and tested action that can be taken when the service is no longer able to meet its SLA, or when that is imminent. Arguably referring to graceful degradation as a "big red switch" is a slightly confusing nomenclature but what is meant is the ability to shed non-critical load in an emergency. The concept of a big red switch is to keep the vital processing progressing while shedding or delaying some non-critical workload. By design, this should never happen, but it's good to have recourse when it does. Trying to figure these out when the service is on fire is risky. If there is some load that can be queued and processed later, it's a candidate for a big red switch. If it's possible to continue to operate the transaction system while disabling advance querying, that's also a good candidate. The key thing is determining what is minimally required if the system is in trouble, and implementing and testing the option to shut off the non-essential services when that happens. Note that a correct big red switch is reversible. Resetting the switch should be tested to ensure that the full service returns to operation, including all batch jobs and other previously halted non-critical work.

*  支持“逼格红色开关”。 “逼格红色开关”这个想法最初来自Windows Live Search，它非常厉害。众所周知，事务性服务与搜索在很大程度上不同。但这个想法非常强大，几乎适用于任何地方。一般来说，“逼格红色开关”是设计和测试阶段，当服务不再能够或者即将不再能够满足其SLA时而采取的操作。严格意义上来讲，以“逼格红色开关”来代指优雅降级有混淆概念的嫌疑，然而，这意味着在紧急情况下它具有分离非关键负载的能力。“逼格红色开关”是指减少或延迟一些非关键工作负载的处理，以保持关键业务的处理。根据设计，它应该永远也用不到，但如果它能做到让我们有所依靠岂不更好。在服务危急关头才尝试找出那些非关键负载是危险的。如果有一些负载可以排队或延迟处理，这就是一个“逼格红色开关”的候选对象。如果禁用高级查询后仍可以继续操作事务系统，这也是一个很好的候选对象。关键的是确定系统有麻烦时什么是最低限度的需求项，执行并测试这些选项，以便真发生时可以关闭那些非必要的服务。注意，正确的“逼格红色开关”是可逆的。应当测试并确保开关复位后完整服务得以恢复，包括所有批处理作业以及其他先前中止的那些非关键作业。

*  Control admission. The second important concept is admission control. If the current load cannot be processed on the system, bringing more work load into the system just assures that a larger cross section of the user base is going to get a bad experience. How this gets done is dependent on the system and some can do this more easily than others. As an example, the last service we led processed email. If the system was over-capacity and starting to queue, we were better off not accepting more mail into the system and let it queue at the source. The key reason this made sense, and actually decreased overall service latency, is that as our queues built, we processed more slowly. If we didn't allow the queues to build, throughput would be higher. Another technique is to service premium customers ahead of non-premium customers, or known users ahead of guests, or guests ahead of users if "try and buy" is part of the business model.

*  准入控制。第二个重要概念是准入控制。如果系统不能承受，引入更多的工作负载只会使得更大面积的用户群产生负面的用户体验。各个系统如何做到这一点是不同的，有些系统可能更容易实现。以我们之前负责的电子邮件服务来举例。如果系统过载并开始排队，那么最好不要接受更多的邮件，而是让它们在源端排队。这样做是有意义的，它实际上减少了整体服务的延迟，这背后的关键因素原因在于：一旦建立了队列，你会处理得更慢。而如果我们不允许建立队列，则吞吐量会更高。另一种技术是为高级用户提供服务的优先级高于非优质用户，或者注册用户的优先级高于访客，或者在“先试后买”的商业模式下访客优先于注册用户。

*  Meter admission. Another incredibly important concept is a modification of the admission control point made above. If the system fails and goes down, be able to bring it back up slowly ensuring that all is well. It must be possible to let just one user in, then let in 10 users/second, and slowly ramp up. It's vital that each service have a fine-grained knob to slowly ramp up usage when coming back on line or recovering from a catastrophic failure. This capability is rarely included in the first release of any service. Where a service has clients, there must be a means for the service to inform the client that it's down and when it might be up. This allows the client to continue to operate on local data if applicable, and getting the client to back-off and not pound the service can make it easier to get the service back on line. This also gives an opportunity for the service owners to communicate directly with the user (see below) and control their expectations. Another client-side trick that can be used to prevent them all synchronously hammering the server is to introduce intentional jitter and per-entity automatic backup.

*  计量准入。这是对上述准入控制点进行改进后产生的另一个至关重要的概念。系统在发生故障并导致宕机后，应当缓慢地恢复以确保一切正常。先让一个用户上线，然后10个用户/秒，接着再慢慢上升。提供细粒度的控制手段以便在重新上线或从灾难性故障中恢复时缓慢提高使用率，这对每个服务都至关重要。在任何服务的第一个版本中就包含此手段是一种绝佳的做法(rarely是指极少还是指绝佳？)。在具有客户端的情况下，必须有手段来通知客户端服务已经停止并告知何时可能恢复。这允许客户端继续对本地数据进行操作（如果适用），同时让客户端主动退避以免冲击服务，从而使得服务更容易恢复。这也为服务所有者提供了与用户直接沟通并控制其期望的机会（见下文）。除此以外，客户端还可以通过引入有意的抖动以及自动备份来防止同步冲击服务器。

## Customer and Press Communication Plan
## 客户与媒体沟通计划

Systems fail, and there will be times when latency or other issues must be communicated to customers. Communications should be made available through multiple channels in an opt-in basis: RSS, web, instant messages, email, etc. For those services with clients, the ability for the service to communicate with the user through the client can be very useful. The client can be asked to back off until some specific time or for some duration. The client can be asked to run in disconnected, cached mode if supported. The client can show the user the system status and when full functionality is expected to be available again.

系统故障、某些潜在风险或其他问题必须与客户沟通。沟通应当通过多种渠道有效执行，基础渠道包括：RSS、网站，即时消息，电子邮件等。对于那些有客户端的服务，通过客户端实现与用户沟通是非常有效的。客户端沟通可以执行到指定时间或者执行指定的一段时间后恢复。 如果支持的话，客户端沟通最好能在连接断开时以缓存模式运行。沟通时，客户端可以向用户显示系统状态以及完整功能预计何时恢复可用。

Even without a client, if users interact with the system via web pages for example, the system state can still be communicated to them. If users understand what is happening and have a reasonable expectation of when the service will be restored, satisfaction is much higher. There is a natural tendency for service owners to want to hide system issues but, over time, we've become convinced that making information on the state of the service available to the customer base almost always improves customer satisfaction. Even in no-charge systems, if people know what is happening and when it'll be back, they appear less likely to abandon the service.

即使没有客户端，如果用户通过网站等其他方式与系统交互，则系统状态仍然可以告知他们。如果用户了解发生了什么，并且对何时恢复服务有合理的预期，那么他们的满意度要高得多。服务所有者天然倾向于隐藏系统问题，但是，随着时间的推移，我们已经确信，向客户群提供关于服务状态的信息几乎总能提高客户满意度。即使在免费系统中，如果人们知道发生了什么、什么时候会恢复，他们似乎不太会因此而放弃这些服务。

Certain types of events will bring press coverage. The service will be much better represented if these scenarios are prepared for in advance. Issues like mass data loss or corruption, security breach, privacy violations, and lengthy service down-times can draw the press. Have a communications plan in place. Know who to call when and how to direct calls. The skeleton of the communications plan should already be drawn up. Each type of disaster should have a plan in place on who to call, when to call them, and how to handle communications.

有些类型的事件会引起媒体的关注。如果有预案，服务会表现得更好。诸如大量数据丢失或损坏、安全漏洞、侵犯隐私以及长时间宕机等问题可能吸引到媒体。制定沟通计划。必须知道通知谁、什么时候通知以及如何通知。沟通计划的框架应该提前制定。每种类型的灾难都应该有一个计划，包括通知谁、什么时候通知以及如何通知。

## Customer Self-Provisioning and Self-Help
## 客户自营和自助

Customer self-provisioning substantially reduces costs and also increases customer satisfaction. If a customer can go to the web, enter the needed data and just start using the service, they are happier than if they had to waste time in a call processing queue. We've always felt that the major cell phone carriers miss an opportunity to both save and improve customer satisfaction by not allowing self-service for those that don't want to call the customer support group.

客户自行配置大大降低了成本，也提高了客户满意度。客户可以访问网站，输入所需的数据，并开始使用服务，比让他们浪费时间排队等待更快乐。我们一直认为，主流手机运营商不为那些不想打客服电话的客户提供自助服务，从而丧失了一个保持和提高客户满意度的机会。

# Conclusion
# 结论

Reducing operations costs and improving service reliability for a high scale internet service starts with writing the service to be operations-friendly. In this document we define operations-friendly and summarize best practices in service design, development, deployment, and operation from engineers working on high-scale services.

降低运营成本并提高大规模互联网服务的服务可靠性，首先是将服务设计为运维友好的。在本文中，我们定义了运维友好，并从为大规模服务进行服务设计、开发、部署和运维的工程师处总结了最佳实践。

# Acknowledgements
# 致谢

We would like to thank Andrew Cencini (Rackable Systems), Tony Chen (Xbox Live), Filo D'Souza (Exchange Hosted Services & SQL Server), Jawaid Ekram (Exchange Hosted Services & Live Meeting), Matt Gambardella (Rackable Systems), Eliot Gillum (Windows Live Hotmail), Bill Hoffman (Windows Live Storage Platform), John Keiser (Windows Live Search), Anastasios Kasiolas (Windows Live Storage), David Nichols (Windows Live Messenger & Silverlight), Deepak Patil (Windows Live Operations), Todd Roman (Exchange Hosted Services), Achint Srivastava (Windows Live Search), Phil Smoot (Windows Live Hotmail), Yan Leshinsky (Windows Live Search), Mike Ziock (Exchange Hosted Services & Live Meeting), Jim Gray (Microsoft Research), and David Treadwell (Windows Live Platform Services) for background information, points from their experience, and comments on early drafts of this paper. We particularly appreciated the input from Bill Hoffman of the Windows Live Storate team and Achint Srivastava and John Keiser, both of the Windows Live Search team.

我们要感谢Andrew Cencini（Rackable Systems）、Tony Chen（Xbox Live）、Filo D'Souza（Exchange Hosted Services和SQL Server），Jawaid Ekram（Exchange Hosted Services和LiveMeeting），Matt Gambardella（Rackable Systems） Eliot Gillum（Windows Live Hotmail），Bill Hoffman（Windows Live Storage Platform），John Keizer（Windows Live Search），Anastasios Kasiolas（Windows Live Storage），David Nichols（Windows Live Messenger和Silverlight），Deepak Patil ，Todd Roman（Exchange Hosted Services），Achint Srivastava（Windows Live Search），Phil Smoot（Windows Live Hotmail），Yan Leshinsky（Windows Live Search），Mike Ziock（Exchange Hosted Services和Live Meeting）以及David Treadwell（Windows Live Platform Services）的提供的背景信息、经验以及对本文初稿的评论。 我们特别鸣谢Windows Live Storate团队的Bill Hoffman、Windows Live Search团队的Achint Srivastava和John Keizer的意见。


# References
# 参考文献

[1] Isard, Michael, "Autopilot: Automatic Data Center Operation," Operating Systems Review, April, 2007, http://research.microsoft.com/users/misard/papers/osr2007.pdf .

[2] Patterson, David, Recovery Oriented Computing, Berkeley, CA, 2005, http://roc.cs.berkeley.edu/ .

[3] Patterson, David, Recovery Oriented Computing: A New Research Agenda for a New Century, February, 2002, http://www.cs.berkeley.edu/̃pattrsn/talks/HPCAkeynote.ppt .

[4] Fox, Armando and D. Patterson, "Self-RepairingComputers," Scientific American, June, 2003, http://www.sciam.com/article.cfm?articleID=000DAA41-3B4E-1EB7-BDC0809EC588EEDF .

[5] Fox, Armand, Crash-Only Software, Stanford, CA, 2004, http://crash.stanford.edu/ .

[6] Hoffman, Bill, Windows Live Storage Platform, private communication, 2006.

[7] Shakib, Darren, Windows Live Search, private communication, 2004.
